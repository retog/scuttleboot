/* Copyright (C) 2020 The Manyverse Authors.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import {StyleSheet} from 'react-native';
import {Dimensions as Dimens} from '../../../global-styles/dimens';
import { Dimensions } from "react-native";

export const styles = StyleSheet.create({
  webView: {
    minWidth: 120,
    width: Dimensions.get('window').width,
    marginTop: Dimens.toolbarHeight, 
    flex: 1,
    marginBottom: Dimens.verticalSpaceNormal,
    alignSelf: 'stretch',
  }
});
