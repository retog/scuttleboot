/* Copyright (C) 2020-2021 The Manyverse Authors.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import {SSBSource} from '../../../drivers/ssb';
import {NavSource} from 'cycle-native-navigation';

export type State = {
  isVisible: boolean;
};

export default function model(ssbSource: SSBSource, navSource: NavSource) {
  const apps$ = ssbSource.apps$.map(
    (getReadable) =>
      function setPrivateFeedReducer(prev: State): State {
        return {...prev};
      },
  );

  return apps$;
}
