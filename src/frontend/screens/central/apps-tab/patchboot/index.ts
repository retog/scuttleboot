import {WebView} from 'react-native-webview';
const MRPC = require('muxrpc');
import manifest from '../../../../ssb/manifest';
//import { default as pull } from 'pull-stream'
const pull = require('pull-stream');
import {h} from '@cycle/react';
//const blobIdToUrl = require('ssb-serve-blobs/id-to-url');
const React = require('react');

export default function getPatchBootView(sbot: any) {
  sbot.conn.start();
  sbot.dhtInvite.start();
  let onLoad: ((_: any) => void) | undefined = undefined;

  class PatchBootWebView extends WebView {
    constructor(props: any) {
      super(props);

      function asyncifyManifest(manifest: any) {
        if (typeof manifest !== 'object' || manifest === null) return manifest;
        let asyncified = {};
        for (let k in manifest) {
          var value = manifest[k];
          // Rewrite re-exported sync methods as async
          if (value === 'sync') {
            value = 'async';
          }
          asyncified[k] = value;
        }
        return asyncified;
      }

      const server = MRPC(null, asyncifyManifest(manifest))(sbot);
      let buffer: any[] = [];
      let pdCallback: null | ((abort: any, value: any) => void) = null;
      this['onMessage'] = (event: any) => {
        const data = JSON.parse(event.nativeEvent.data);
        //console.log('got data from page', String.fromCharCode(data));
        const asBuffer = Buffer.from(data);
        //console.log('got buffer from page', asBuffer.length);
        if (pdCallback) {
          const cb = pdCallback;
          pdCallback = null;
          cb(null, asBuffer);
        } else {
          buffer.push(asBuffer);
        }
      };
      const fromPage = function (
        abort: any,
        cb: (abort: any, value: any) => void,
      ) {
        if (buffer.length > 0) {
          const data = buffer[0];
          buffer = buffer.splice(1);
          cb(null, data);
        } else {
          pdCallback = cb;
        }
      };

      const toPage = (
        reader: (abort: any, cb: (abort: any, value: any) => void) => void,
      ) => {
        const more = (abort: any, value: any) => {
          if (abort) return;
          // console.log('got message to send to page', abort, String.fromCharCode(...value));
          const messageSender = `
          window.postMessage({
            direction: 'from-content-script',
            message: ${JSON.stringify([...value])}
          }, '*')
          `;
          this.injectJavaScript(messageSender);
          reader(null, more);
        };
        reader(null, more);
      };

      const sendPing = () => {
        const script = `
        window.postMessage({
          direction: 'from-content-script',
          action: 'ping',
        }, '*')
        `;
        this.injectJavaScript(script);
      };

      /*function logger(text: string) {
        return pull.map((v: any) => {
          console.log(text,v)
          return v
        })
      }*/

      pull(fromPage, server.createStream(), toPage);
      //(this as any).setMessagingEnabled(true);
      const pageScript = `
      window.addEventListener('message', (event) => {
        if (event.source == window &&
            event.data &&
            event.data.direction == 'from-page-script') {
          if (event.data.action == 'ping') {
            window.postMessage({
              direction: 'from-content-script',
              action: 'ping',
            }, '*')
          } else {
            let str = '['+event.data.message+']';// //new TextDecoder().decode(event.data.message)
            window.ReactNativeWebView.postMessage(str);
          }
        }
      })
      `;
      onLoad = (event: any) => {
        sendPing();
        this.injectJavaScript(pageScript);
      };
    }

    isPBW = true;
  }

  /*const blob = '&SJACmcUX3x8j0HjZTBLiYt4O5j2jeXMwyyeEmDsBWS4=.sha256';
  const blobUri = blobIdToUrl(blob, {
    contentType: 'text/html',
  });
  const blobPromise = sbot.blobs.want(blob);//.then(() => 'blob ready');*/

  const scuttleUrlPromise = new Promise((resolve, reject) => {
    sbot.settingsUtils.read((err: any, value: any) => {
      if (err) reject(err);
      else resolve(value);
    });
  }).then((s: any) => (console.log(s), s.scuttleUrl));

  let PatchBootView: ({style}: {style: any}) => React.ReactElement | null;

  PatchBootView = ({style}: {style: any}) => {
    const [scuttleUrl, setScuttleUrl] = React.useState('');
    React.useEffect(() => {
      const fetchScuttleUrl = async () => {
        console.log('---');
        const scuttleUrl = await scuttleUrlPromise;
        console.log('scuttleUrl:', scuttleUrl);
        setScuttleUrl(scuttleUrl);
      };
      fetchScuttleUrl();
    }, []);
    if (scuttleUrl === '') return null;
    return h(PatchBootWebView, {
      source: {
        uri: scuttleUrl,
      },
      onMessage: (event: any) => {
        //this prop must be defined, method will be redefined on instance
      },
      onLoad: (event: any) => {
        if (onLoad) onLoad(event);
      },
      style,
    });
  };
  return PatchBootView;
}
