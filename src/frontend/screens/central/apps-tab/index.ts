/* Copyright (C) 2020-2021 The Manyverse Authors.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import {Stream} from 'xstream';
import {ReactElement} from 'react';
import {ReactSource} from '@cycle/react';
import {StateSource, Reducer} from '@cycle/state';
import {NavSource} from 'cycle-native-navigation';
import {SSBSource} from '../../../drivers/ssb';
import {State} from './model';
import model from './model';
import view from './view';

export type Sources = {
  screen: ReactSource;
  state: StateSource<State>;
  navigation: NavSource;
  ssb: SSBSource;
};

export type Sinks = {
  screen: Stream<ReactElement<any>>;
  state: Stream<Reducer<State>>;
};

export function appsTab(sources: Sources): Sinks {
  const reducer$ = model(sources.ssb, sources.navigation);
  const vdom$ = view(sources.ssb);

  return {
    screen: vdom$,
    state: reducer$,
  };
}
