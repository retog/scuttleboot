/* Copyright (C) 2020-2021 The Manyverse Authors.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import {h} from '@cycle/react';
import {styles} from './styles';
import getPatchBootView from './patchboot';
import {SSBSource} from '../../../drivers/ssb';

export default function view(ssbSource: SSBSource) {
  return ssbSource.ssb$.map((ssbClient) =>
    h(getPatchBootView(ssbClient), {
      style: styles.webView,
    }),
  );
}
